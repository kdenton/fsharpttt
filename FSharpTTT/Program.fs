﻿// Learn more about F# at http://fsharp.org

open System
open TicTacToe

[<EntryPoint>]
let main argv =
    let state = createBoard 3
    let playerOne = Human "X"
    let playerTwo = Computer "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    
    play state playerState (Console.ReadLine, Console.Out)
    0 // return an integer exit code
