module TicTacToe

open System
open System.IO
open System.Text

[<Literal>]
let WinGame = "win"
[<Literal>]
let TieGame = "tie"
[<Literal>]
let TieGameMessage = "Game is tied"
[<Literal>]
let InvalidInput = "Please enter a number between 1 and 9"
[<Literal>]
let CellTaken = "Please enter a cell that hasn't been taken"

type Player =
    | Human of string
    | Computer of string

type Cell = {
    value : Player Option
}
 
type GameStatus = {
    isOver : bool
    status : string Option
}

type Move = 
    | Valid of int
    | Invalid of string
    
type PlayerState = {
    currentPlayer : Player
    playerOne : Player
    playerTwo : Player
    }
    
type Node = {
    position : int
    }
    
let printOutput output textToOutput =
    Console.SetOut(output)
    printfn "%s" textToOutput
    output.Flush()

let square n = 
    n * n

let getBoardSize (board : Cell list) =
    board.Length |> float |> sqrt |> int
    
let createBoard size =
    let squared = size |> square
    [1..squared]
    |> List.map(fun x -> {value = None})
    
let getPlayerSymbol player =
    match player with
    | Human s -> s
    | Computer s -> s
    
let serializeBoard (cells : Cell list) = 
    let newCells = cells 
                    |> List.mapi(fun i x -> match x.value with 
                                            | Some s -> getPlayerSymbol s 
                                            | None -> string (i + 1))

    [   "   |   |   ";
        " " + newCells.[0] + " | " + newCells.[1] + " | " + newCells.[2] + " ";
        "___|___|___";
        "   |   |   ";
        " " + newCells.[3] + " | " + newCells.[4] + " | " + newCells.[5] + " ";
        "___|___|___";
        "   |   |   ";
        " " + newCells.[6] + " | " + newCells.[7] + " | " + newCells.[8] + " ";
        "   |   |   "]
    |> String.concat(Environment.NewLine)

let replaceCell (cell : Cell) cellIndex index value =
    if cellIndex = index then
        {value = Some value}
    else
        cell
        
let buildBoardFromExisting (cells : Cell list) index value =
    cells 
    |> List.mapi(fun i x -> replaceCell x i index value)
    
let parseInt (s : string) =
    match System.Int32.TryParse s with
        | true, n ->  n
        | false, _ -> -1
        
let cellTaken (cells : Cell list) move playerState =
    cells.[move - 1].value = Some playerState.playerOne || 
    cells.[move - 1].value = Some playerState.playerTwo
    
let invalidMove output toPrint cells =
    printOutput output (serializeBoard cells)
    printOutput output toPrint
    
let validateMove (cells : Cell list) output move playerState = 
    match parseInt move with
        | n when n < 1 -> Invalid InvalidInput
        | n when n > 9 -> Invalid InvalidInput
        | n when cellTaken cells n playerState -> Invalid CellTaken
        | n -> Valid n
        
let getColumns (state : Cell list) =
    let boardSize = getBoardSize state
    
    state
    |> List.chunkBySize boardSize
    |> List.mapi(fun i x -> List.mapi(fun n _ -> state.[(i + (n * boardSize))]) x)
    
let getDiagonals (state : Cell list) =
    let size = getBoardSize state
    let diagonal = state
                    |> List.chunkBySize size
                    |> List.mapi(fun i x -> x.[i])
    let otherDiagonal = state
                        |> List.chunkBySize size
                        |> List.rev
                        |> List.mapi(fun i x -> x.[i])
                        
    [diagonal; otherDiagonal]
    
let checkRowWin (state : Cell list) currentPlayer = 
    state 
    |> List.splitInto 3
    |> List.exists(fun x -> x |> List.forall(fun y -> y.value = Some currentPlayer))
    
let checkColumnWin (state : Cell list) currentPlayer =
    state 
    |> getColumns
    |> List.exists(fun x -> x |> List.forall(fun y -> y.value = Some currentPlayer))
    
let checkDiagonalWin (state : Cell list) currentPlayer =
    state
    |> getDiagonals
    |> List.exists(fun x -> x |> List.forall(fun y -> y.value = Some currentPlayer))

let checkGameIsTied (state : Cell list) playerState =
    state 
    |> List.where(fun x -> x.value <> Some playerState.playerOne &&
                           x.value <> Some playerState.playerTwo)
    |> List.isEmpty 
    
let hasWon state currentPlayer =
    checkRowWin state currentPlayer || 
    checkDiagonalWin state currentPlayer ||
    checkColumnWin state currentPlayer
        
let getPlayerState playerState = 
    if playerState.currentPlayer = playerState.playerOne then
        {currentPlayer = playerState.playerTwo; 
            playerOne = playerState.playerOne; 
            playerTwo = playerState.playerTwo} 
    else 
        {currentPlayer = playerState.playerOne; 
                    playerOne = playerState.playerOne; 
                    playerTwo = playerState.playerTwo} 
    
let getAvailableNodes (state : Cell list) playerState =
    let nodes = state
                |> List.mapi(fun i x -> match x.value with 
                                         | Some s -> {position = -1} 
                                         | None -> {position = i})
                |> List.filter(fun x -> x.position <> -1)
    
    if hasWon state playerState.playerOne ||
       hasWon state playerState.playerTwo then
        []
    else
        nodes
    
let getOpponentPlayer playerState currentPlayer =
    if playerState.playerOne = currentPlayer then
        playerState.playerTwo
    else
        playerState.playerOne
    
let getScore state originatingPlayer playerState =
    if hasWon state originatingPlayer then
        10
    elif hasWon state (getOpponentPlayer playerState originatingPlayer) then
        -10
    else
        0
    
let rec minimax state originalPlayerState playerState node nodeList =
    let newNodeList = nodeList |> List.append([node])
    let nodes = getAvailableNodes state playerState
    
    if nodes.Length = 0 then
        (getScore state originalPlayerState.currentPlayer playerState, newNodeList.[newNodeList.Length - 2])
    elif playerState.currentPlayer = originalPlayerState.currentPlayer then
        nodes
        |> List.mapi(fun i x -> (minimax (buildBoardFromExisting state (x.position) playerState.currentPlayer) originalPlayerState (originalPlayerState |> getPlayerState) x newNodeList, x))
        |> List.maxBy fst |> fst
    else
        nodes
        |> List.map(fun x -> (minimax (buildBoardFromExisting state (x.position) playerState.currentPlayer) originalPlayerState originalPlayerState x newNodeList, x))
        |> List.minBy fst |> fst 
    
let rec getComputerNextMove playerState (cells : Cell list) = 
    let index = (snd (minimax cells playerState playerState {position = 0} [])).position
    
    match cells.[index].value with
    | Some s -> getComputerNextMove playerState cells
    | None -> string (index + 1)

let getHumanNextMove (input : unit -> string, output) playerSymbol = 
    printOutput output ("Player " + playerSymbol + "'s turn.")
    printOutput output ("Enter cell to place your " + playerSymbol)
    
    input()
    
let rec getNextMove (input : unit -> string, output) playerState (cells : Cell list) =
    let nextMove = match playerState.currentPlayer with 
                    | Human h -> getHumanNextMove (input, output) h
                    | Computer c -> getComputerNextMove playerState cells
    
    match validateMove cells output nextMove playerState with
        | Invalid s -> 
            invalidMove output s cells
            getNextMove (input, output) playerState cells
        | Valid n -> n - 1 
        
let gameIsOver state playerState = 
    if hasWon state playerState.currentPlayer then
        {isOver = true; status = Some WinGame}
    elif checkGameIsTied state playerState then
        {isOver = true; status = Some TieGame}
    else
        {isOver = false; status = None}
        
let rec play state playerState (input : unit -> string, output) =
    printOutput output (serializeBoard state)
    let nextMove = getNextMove (input, output) playerState state
    let newState = buildBoardFromExisting state nextMove playerState.currentPlayer
    
    let status = gameIsOver newState playerState
    if status.isOver then
        printOutput output (serializeBoard newState)
        if status.status = Some WinGame then
            let overMessage = "Player " + getPlayerSymbol playerState.currentPlayer + " has won"
            printOutput output overMessage
        else
            let overMessage = TieGameMessage
            printOutput output overMessage
    else
        let newPlayerState = getPlayerState playerState
        play newState newPlayerState (input, output)