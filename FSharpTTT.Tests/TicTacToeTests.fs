module TicTacToeTests

open Xunit
open TicTacToe
open System
open System.IO
open TestHelpers

[<Fact>]
let ``createBoard returns a list of Cells based on provided int`` () =
    let expectedCellList = [1..9] |> List.map(fun x -> {value = None})
    let cellList = createBoard 3
    
    Assert.Equal(9, cellList.Length)
    Assert.Equal<Cell list>(expectedCellList, cellList)
    
[<Fact>]
let ``serializeBoard returns a string visual of the board state`` () =
    let expectedBoard = [    "   |   |   ";
                             " 1 | 2 | 3 ";
                             "___|___|___";
                             "   |   |   ";
                             " 4 | 5 | 6 ";
                             "___|___|___";
                             "   |   |   ";
                             " 7 | 8 | 9 ";
                             "   |   |   "]
                               |> String.concat(Environment.NewLine)
                
    let serializedBoard = createBoard 3 |> serializeBoard
    
    Assert.Equal(expectedBoard, serializedBoard)
    
[<Fact>]
let ``buildBoardFromExisting returns a list of cells based on old updated with new value`` () =
    let playerOne = Human "X"
    let existingBoard = createBoard 3
    let expectedBoard = [{value = None}; {value = None}; {value = Some playerOne};
                        {value = None}; {value = None}; {value = None};
                        {value = None}; {value = None}; {value = None}]
    
    Assert.Equal<Cell list>(expectedBoard, buildBoardFromExisting existingBoard 2 playerOne)
    
[<Fact>]
let ``getCurrentPlayer returns the opposite of the X player`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let expectedPlayerState = {currentPlayer = playerTwo; playerOne = playerOne; playerTwo = playerTwo}
    
    Assert.Equal(expectedPlayerState, getPlayerState playerState)
    
[<Fact>]
let ``checkRowWin if player has 3 in a row returns true`` () =
    let playerOne = Human "X"
    let state = [{value = Some playerOne}; {value = Some playerOne}; {value = Some playerOne};
                {value = None}; {value = None}; {value = None};
                {value = None}; {value = None}; {value = None}]
    
    Assert.True(checkRowWin state playerOne)
    
[<Fact>]
let ``checkRowWin if player does not have 3 in a row returns false`` () =
    let playerOne = Human "X"
    let state = [{value = Some playerOne}; {value = None}; {value = Some playerOne};
                {value = None}; {value = None}; {value = None};
                {value = None}; {value = None}; {value = None}]
    
    Assert.False(checkRowWin state playerOne)
    
[<Fact>]
let ``play if player has three in a row it prints the win message`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = Some playerOne}; {value = None}; {value = Some playerOne};
                {value = None}; {value = None}; {value = None};
                {value = None}; {value = None}; {value = None}]
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    let inputs() = "2"
    let expectedValue = "   |   |   
                          X | 2 | X 
                         ___|___|___
                            |   |   
                          4 | 5 | 6 
                         ___|___|___
                            |   |   
                          7 | 8 | 9 
                            |   |   
                         Player X's turn.
                         Enter cell to place your X
                            |   |   
                          X | X | X 
                         ___|___|___
                            |   |   
                          4 | 5 | 6 
                         ___|___|___
                            |   |   
                          7 | 8 | 9 
                            |   |   
                         Player X has won"
    
    play state playerState (inputs, sw)
    
    stream.Position <- int64(0)
    Assert.True(compareStringsNoWhitespace expectedValue (sr.ReadToEnd()))
    
[<Fact>]
let ``play if the game is tied it prints the tie game message`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = Some playerOne}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerTwo}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerOne}; {value = None}; {value = Some playerTwo}]
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    let inputs() = "8"
    let expectedValue = "   |   |   
                          X | O | X 
                         ___|___|___
                            |   |   
                          O | O | X 
                         ___|___|___
                            |   |   
                          X | 8 | O 
                            |   |   
                         Player X's turn.
                         Enter cell to place your X
                            |   |   
                          X | O | X 
                         ___|___|___
                            |   |   
                          O | O | X 
                         ___|___|___
                            |   |   
                          X | X | O 
                            |   |   
                         Game is tied"
    
    play state playerState (inputs, sw)
    
    stream.Position <- int64(0)
    Assert.True(compareStringsNoWhitespace expectedValue (sr.ReadToEnd()))
    
[<Fact>]
let ``gameIsOver returns type with correct properties if game is won by row`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = None}; {value = None};
                {value = Some playerOne}; {value = Some playerOne}; {value = Some playerOne};
                {value = None}; {value = None}; {value = None}]
    let expectedType = {isOver = true; status = Some WinGame}
    
    Assert.Equal(expectedType, gameIsOver state playerState)
    
[<Fact>]
let ``gameIsOver returns type with correct properties if game is won by column`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = None}; {value = Some playerOne};
                {value = None}; {value = None}; {value = Some playerOne};
                {value = None}; {value = None}; {value = Some playerOne}]
    let expectedType = {isOver = true; status = Some WinGame}
    
    Assert.Equal(expectedType, gameIsOver state playerState)
    
[<Fact>]
let ``gameIsOver returns type with correct properties if game is not over by row`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = None}; {value = None};
                {value = None}; {value = Some playerOne}; {value = Some playerOne};
                {value = None}; {value = None}; {value = None}]
    let expectedType = {isOver = false; status = None}
    
    Assert.Equal(expectedType, gameIsOver state playerState)
    
[<Fact>]
let ``gameIsOver returns type with correct properties if game is tied`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = Some playerOne}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerTwo}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerOne}; {value = Some playerOne}; {value = Some playerTwo}]
    let expectedType = {isOver = true; status = Some TieGame}
    
    Assert.Equal(expectedType, gameIsOver state playerState)
    
[<Fact>]
let ``gameIsOver returns type with correct properties if game is won by diagonal`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = None}; {value = Some playerOne};
                {value = None}; {value = Some playerOne}; {value = None};
                {value = Some playerOne}; {value = None}; {value = None}]
    let expectedType = {isOver = true; status = Some WinGame}
    
    Assert.Equal(expectedType, gameIsOver state playerState)
    
[<Fact>]
let ``checkGameIsTied return true if there are no available cells`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = Some playerOne}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerTwo}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerOne}; {value = Some playerOne}; {value = Some playerTwo}]
    
    Assert.True(checkGameIsTied state playerState)
    
[<Fact>]
let ``checkGameIsTied return false if there are any available cells`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = Some playerOne}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerTwo}; {value = None}; {value = Some playerOne};
                {value = Some playerOne}; {value = Some playerOne}; {value = Some playerTwo}]
    
    Assert.False(checkGameIsTied state playerState)
    
[<Fact>]
let ``validateMove non number string returns Invalid with correct message`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = None}; {value = None};
                 {value = None}; {value = Some playerOne}; {value = None};
                 {value = None}; {value = None}; {value = None}]
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    let expectedValue = "   |   |   
                          1 | 2 | 3 
                         ___|___|___
                            |   |   
                          4 | X | 6 
                         ___|___|___
                            |   |   
                          7 | 8 | 9 
                            |   |   
                         Please enter a number between 1 and 9"
    
    Assert.Equal(Invalid InvalidInput, validateMove state sw "test" playerState)
    
[<Fact>]
let ``validateMove move less than one returns Invalid with correct message`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = None}; {value = None};
                 {value = None}; {value = Some playerOne}; {value = None};
                 {value = None}; {value = None}; {value = None}]
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    let expectedValue = "   |   |   
                          1 | 2 | 3 
                         ___|___|___
                            |   |   
                          4 | X | 6 
                         ___|___|___
                            |   |   
                          7 | 8 | 9 
                            |   |   
                         Please enter a number between 1 and 9"
    
    Assert.Equal(Invalid InvalidInput, validateMove state sw "0" playerState)
    
[<Fact>]
let ``validateMove move greater than nine returns Invalid with correct message`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None}]
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    let expectedValue = "   |   |   
                          1 | 2 | 3 
                         ___|___|___
                            |   |   
                          4 | 5 | 6 
                         ___|___|___
                            |   |   
                          7 | 8 | 9 
                            |   |   
                         Please enter a number between 1 and 9"
    
    Assert.Equal(Invalid InvalidInput, validateMove state sw "10" playerState)
    
[<Fact>]
let ``validateMove valid move returns Valid with correct move number`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = Some playerTwo}; {value = None};
                 {value = None}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None}]
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    
    Assert.Equal(Valid 1, validateMove state sw "1" playerState)
    
[<Fact>]
let ``validateMove move is a taken cell returns Invalid with correct message`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = None}; {value = Some playerTwo}; {value = None};
                 {value = None}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None}]
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    let expectedValue = "   |   |   
                          1 | O | 3 
                         ___|___|___
                            |   |   
                          4 | 5 | 6 
                         ___|___|___
                            |   |   
                          7 | 8 | 9 
                            |   |   
                         Please enter a cell that hasn't been taken"
    
    Assert.Equal(Invalid CellTaken, validateMove state sw "2" playerState)
    
[<Fact>]
let ``getColumns returns a list of cell lists with correct columns`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = Some (Human "1")}; {value = Some (Human "2")}; {value = Some (Human "3")};
                 {value = Some (Human "4")}; {value = Some (Human "5")}; {value = Some (Human "6")};
                 {value = Some (Human "7")}; {value = Some (Human "8")}; {value = Some (Human "9")}]
    let expected = [[{value = Some (Human "1")}; {value = Some (Human "4")}; {value = Some (Human "7")}];
                    [{value = Some (Human "2")}; {value = Some (Human "5")}; {value = Some (Human "8")}];
                    [{value = Some (Human "3")}; {value = Some (Human "6")}; {value = Some (Human "9")}]]
    
    Assert.Equal<Cell list>(expected, getColumns state)
    
[<Fact>]
let ``checkColumnWin returns false if the current player does not have three in a row in a column`` () =
    let playerOne = Human "X"
    let state = [{value = Some playerOne}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None};
                 {value = Some playerOne}; {value = None}; {value = None}]
    
    Assert.False(checkColumnWin state playerOne)
    
[<Fact>]
let ``checkColumnWin returns true if the current player has three in a row in a column`` () =
    let playerOne = Human "X"
    let state = [{value = None}; {value = Some playerOne}; {value = None};
                 {value = None}; {value = Some playerOne}; {value = None};
                 {value = None}; {value = Some playerOne}; {value = None}]
    
    Assert.True(checkColumnWin state playerOne)

let ``getDiagonals returns a list of Cell lists with the diagonals`` () =
    let state = [{value = Some (Human "1")}; {value = Some (Human "2")}; {value = Some (Human "3")};
                 {value = Some (Human "4")}; {value = Some (Human "5")}; {value = Some (Human "6")};
                 {value = Some (Human "7")}; {value = Some (Human "8")}; {value = Some (Human "9")}]
    let expected = [[{value = Some (Human "1")}; {value = Some (Human "5")}; {value = Some (Human "9")}];
                    [{value = Some (Human "7")}; {value = Some (Human "5")}; {value = Some (Human "3")}]]
                    
    Assert.Equal<Cell list>(expected, getDiagonals state)
    
[<Fact>]
let ``checkDiagonalWin returns false if neither diagonal has 3 in a row`` () =
    let playerOne = Human "X"
    let state = [{value = Some playerOne}; {value = None}; {value = Some playerOne};
                 {value = None}; {value = None}; {value = None};
                 {value = Some playerOne}; {value = None}; {value = Some playerOne}]
    
    Assert.False(checkDiagonalWin state playerOne)
    
[<Fact>]
let ``checkDiagonalWin returns true if a diagonal has 3 in a row`` () =
    let playerOne = Human "X"
    let state = [{value = Some playerOne}; {value = None}; {value = Some playerOne};
                 {value = None}; {value = Some playerOne}; {value = None};
                 {value = Some playerOne}; {value = None}; {value = Some playerOne}]
    
    Assert.True(checkDiagonalWin state playerOne)
    
[<Fact>]
let ``play if the next player is a computer player it doesn't ask to input cell`` () =
    let playerOne = Computer "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = Some playerOne}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerTwo}; {value = Some playerTwo}; {value = Some playerOne};
                {value = Some playerOne}; {value = None}; {value = Some playerTwo}]
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    let inputs() = ""
    let expectedValue = "   |   |   
                          X | O | X 
                         ___|___|___
                            |   |   
                          O | O | X 
                         ___|___|___
                            |   |   
                          X | 8 | O 
                            |   |   
                            |   |   
                          X | O | X 
                         ___|___|___
                            |   |   
                          O | O | X 
                         ___|___|___
                            |   |   
                          X | X | O 
                            |   |   
                         Game is tied"
    
    play state playerState (inputs, sw)
    
    stream.Position <- int64(0)
    Assert.True(compareStringsNoWhitespace expectedValue (sr.ReadToEnd()))
    
[<Fact>]
let ``getComputerNextMove returns a valid, not taken cell`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    let state = [{value = Some playerOne}; {value = Some playerTwo}; {value = Some playerOne};
                 {value = Some playerTwo}; {value = Some playerTwo}; {value = Some playerOne};
                 {value = Some playerOne}; {value = None}; {value = Some playerTwo}]
                 
    Assert.Equal("8", getComputerNextMove playerState state)
    
[<Fact>]
let ``getHumanNextMove returns the input value`` () =
    let stream = new MemoryStream()
    let sw = new StreamWriter(stream)
    let sr = new StreamReader(stream)
    let inputs() = "8"
    
    Assert.Equal("8", getHumanNextMove (inputs, sw) "X")
    
[<Fact>]
let ``getAvailableNodes returns a list of available Nodes`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let state = [{value = Some playerOne}; {value = None}; {value = Some playerOne};
                 {value = None}; {value = Some playerTwo}; {value = None};
                 {value = Some playerOne}; {value = None}; {value = Some playerTwo}]
    let expected = [{position = 1}; {position = 3}; {position = 5}; {position = 7}]
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    
    Assert.Equal<Node list>(expected, getAvailableNodes state playerState)
    
[<Fact>]
let ``minimax chooses only available cell that is a tie`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let state = [{value = Some playerOne}; {value = Some playerTwo}; {value = Some playerOne};
                 {value = Some playerTwo}; {value = Some playerTwo}; {value = Some playerOne};
                 {value = Some playerOne}; {value = None}; {value = Some playerTwo}]
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    Assert.Equal((0, {position = 7}), minimax state playerState playerState {position = 0} [])
    
[<Fact>]
let ``minimax wins if possible`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let state = [{value = Some playerOne}; {value = None}; {value = None};
                 {value = Some playerTwo}; {value = Some playerOne}; {value = Some playerOne};
                 {value = None}; {value = Some playerTwo}; {value = Some playerTwo}]
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    Assert.Equal((0, {position = 6}), minimax state playerState playerState {position = 0} [])
    
[<Fact>]
let ``minimax prevents win`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let state = [{value = Some playerOne}; {value = None}; {value = Some playerOne};
                 {value = Some playerTwo}; {value = Some playerTwo}; {value = Some playerOne};
                 {value = Some playerOne}; {value = None}; {value = Some playerTwo}]
    let playerState = {currentPlayer = playerTwo; playerOne = playerOne; playerTwo = playerTwo}
    Assert.Equal((0, {position = 1}), minimax state playerState playerState {position = 0} [])
    
[<Fact>]
let ``minimax creates a fork if possible`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let state = [{value = Some playerOne}; {value = None}; {value = Some playerTwo};
                 {value = Some playerOne}; {value = None}; {value = None};
                 {value = None}; {value = Some playerOne}; {value = None}]
    let playerState = {currentPlayer = playerOne; playerOne = playerOne; playerTwo = playerTwo}
    Assert.Equal((10, {position = 8}), minimax state playerState playerState {position = 0} [])
    
[<Fact>]
let ``minimax blocks opponent fork`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let state = [{value = Some playerOne}; {value = None}; {value = Some playerTwo};
                 {value = Some playerOne}; {value = None}; {value = None};
                 {value = None}; {value = Some playerOne}; {value = None}]
    let playerState = {currentPlayer = playerTwo; playerOne = playerOne; playerTwo = playerTwo}
    Assert.Equal((-10, {position = 8}), minimax state playerState playerState {position = 0} [])
    
[<Fact>]
let ``minimax places in the center if possible`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let state = [{value = Some playerOne}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None}]
    let playerState = {currentPlayer = playerTwo; playerOne = playerOne; playerTwo = playerTwo}
    Assert.Equal((0, {position = 4}), minimax state playerState playerState {position = 0} [])
    
[<Fact>]
let ``minimax places in empty corner if possible`` () =
    let playerOne = Human "X"
    let playerTwo = Human "O"
    let state = [{value = None}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None};
                 {value = None}; {value = None}; {value = None}]
    let playerState = {currentPlayer = playerTwo; playerOne = playerOne; playerTwo = playerTwo}
    Assert.Equal((0, {position = 8}), minimax state playerState playerState {position = 0} [])